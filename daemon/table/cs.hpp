/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014-2021,  Regents of the University of California,
 *                           Arizona Board of Regents,
 *                           Colorado State University,
 *                           University Pierre & Marie Curie, Sorbonne University,
 *                           Washington University in St. Louis,
 *                           Beijing Institute of Technology,
 *                           The University of Memphis.
 *
 * This file is part of NFD (Named Data Networking Forwarding Daemon).
 * See AUTHORS.md for complete list of NFD authors and contributors.
 *
 * NFD is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NFD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NFD, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NFD_DAEMON_TABLE_CS_HPP
#define NFD_DAEMON_TABLE_CS_HPP

#include "cs-policy.hpp"
#include "face/face-endpoint.hpp"
#include "ns3/ndnSIM/model/ndn-common.hpp"
#include "../../../../../json/single_include/nlohmann/json.hpp"
#include "../../../../../HTTPRequest/include/HTTPRequest.hpp"

using json = nlohmann::json;
using string = std::string;


namespace nfd {
namespace cs {

/** \brief implements the Content Store
 *
 *  This Content Store implementation consists of a Table and a replacement policy.
 *
 *  The Table is a container ( \c std::set ) sorted by full Names of stored Data packets.
 *  Data packets are wrapped in Entry objects. Each Entry contains the Data packet itself,
 *  and a few additional attributes such as when the Data becomes non-fresh.
 *
 *  The replacement policy is implemented in a subclass of \c Policy.
 */
class Cs : noncopyable
{
public:
  explicit
  Cs(size_t nMaxPackets = 10);

  /** \brief inserts a Data packet
   */
  void
  insert(const Data& data, bool isUnsolicited = false);

  /** \brief asynchronously erases entries under \p prefix
   *  \tparam AfterEraseCallback `void f(size_t nErased)`
   *  \param prefix name prefix of entries
   *  \param limit max number of entries to erase
   *  \param cb callback to receive the actual number of erased entries; must not be empty;
   *            it may be invoked either before or after erase() returns
   */
  template<typename AfterEraseCallback>
  void
  erase(const Name& prefix, size_t limit,  AfterEraseCallback&& cb)
  {
    size_t nErased = eraseImpl(prefix, limit);
    cb(nErased);
  }

  /** \brief finds the best matching Data packet
   *  \tparam HitCallback `void f(const Interest&, const Data&)`
   *  \tparam MissCallback `void f(const Interest&)`
   *  \param interest the Interest for lookup
   *  \param hit a callback if a match is found; must not be empty
   *  \param miss a callback if there's no match; must not be empty
   *  \note A lookup invokes either callback exactly once.
   *        The callback may be invoked either before or after find() returns
   */
  template<typename HitCallback, typename MissCallback>
  void
  find(const Interest& interest, HitCallback&& hit, MissCallback&& miss) const
  {
    bool TraditionalFind = false ;
    uint32_t context = ns3::Simulator::GetContext() ;


    bool NodeisCosumerOrProduer = false ;
    std::list<int> List = {0,8};
    auto it = std::find(List.begin(), List.end(), context);
    if (it != List.end()) {
        NodeisCosumerOrProduer = true ;
    } 


  if((interest.getName().toUri().find("localhost") != string::npos) || TraditionalFind || NodeisCosumerOrProduer )  
  {
     auto match = findImpl(interest);
     if (match == m_table.end()) {
       miss(interest);
       return;
     }
     hit(interest, match->getData());
     return;
  }

    std::cout << "[FIND] -------------------------- NODE : " << to_string(context) << "----------------------------" << std::endl ; 

    string Model = findAPI(interest) ;

    if(Model.length() == 0 ){
      std::cout << "--------- Forward interest -------------------- " << std::endl ;
      miss(interest) ;
    return ;
    }
////// create data packet /////////
  uint32_t signature = 0 ;

  auto dataModel = make_shared<Data>();
  dataModel->setName(interest.getName());

  MakeTimeAccessor(&Cs::m_freshness);
  dataModel->setFreshnessPeriod(::ndn::time::milliseconds(m_freshness.GetMilliSeconds()));

//// convert model to uint8_t //////
  std::vector<uint8_t> result;

    for (char c : Model) {
        // Convert each character to its ASCII value
        uint8_t asciiValue = static_cast<uint8_t>(c);
        result.push_back(asciiValue);
    }
///////////////////////////////////
    

  dataModel->setContent(result);

  ndn::SignatureInfo signatureInfo(static_cast< ::ndn::tlv::SignatureTypeValue>(255));

  dataModel->setSignatureInfo(signatureInfo);

   ::ndn::EncodingEstimator estimator;
   ::ndn::EncodingBuffer encoder(estimator.appendVarNumber(signature), 0);
   encoder.appendVarNumber(signature);
   dataModel->setSignatureValue(encoder.getBuffer());

  std::cout << "------- Send back data -------------------- " << std::endl ;
  hit(interest, *dataModel);     
  }
  
  string findAPI(const Interest& interest) const {

  if((interest.getName().toUri().find("context") == string::npos))  // if system find 
  {
    return "" ; 
  }
  
  try
   {
    uint32_t context = ns3::Simulator::GetContext() ;
    std::string ConsumerInterest = interest.getName().toUri();
    ConsumerInterest = ConsumerInterest.substr(0, ConsumerInterest.length() - 6);
    ConsumerInterest =  ConsumerInterest  + "/graph_id:" + to_string(context) ;

/////
     size_t startPos = 0;
     std::string toReplace = "%3A" ;
     std::string replacement = "=" ;

    // Find the first occurrence of toReplace
    while ((startPos = ConsumerInterest.find(toReplace, startPos)) != std::string::npos) {
        // Replace toReplace with the replacement string
        ConsumerInterest.replace(startPos, toReplace.length(), replacement);

        // Move the starting position to avoid infinite loop
        startPos += replacement.length();
    }
/////

    for (size_t i = 0; i < ConsumerInterest.length(); i++) {
    if (ConsumerInterest[i] == ':') {
        ConsumerInterest[i] = '=';
    }
}
    std::cout << "--------- Interest : " <<  ConsumerInterest << " ---------------------" << std::endl;

    json jsoninterest ;
    jsoninterest["context"] = ConsumerInterest;
    http::Request request{"http://127.0.0.1:5000/model"};

    const string body = to_string(jsoninterest);

    const auto response = request.send("GET", body, {
        {"Content-Type", "application/json"}
    });

    const string Model = std::string{response.body.begin(), response.body.end()};
    if(Model.length() == 0 ){
      std::cout << "--------- Model not found ----------" << std::endl ;
    }
    else 
    {
      std::cout << "--------- Model found ----------" << std::endl ;
      std::cout <<" -------size of the model--- " << to_string(sizeof(Model)) << " --------" << std::endl ;
      std::cout <<" -------Model--- " << Model << " --------" << std::endl ;
    }
    return Model ;  
}
catch (const std::exception& e)
{
    std::cerr << "Request failed, error: " << e.what() << '\n';
}
}

  /** \brief get number of stored packets
   */
  size_t
  size() const
  {
    return m_table.size();
  }

public: // configuration
  /** \brief get capacity (in number of packets)
   */
  size_t
  getLimit() const
  {
    return m_policy->getLimit();
  }

  /** \brief change capacity (in number of packets)
   */
  void
  setLimit(size_t nMaxPackets)
  {
    return m_policy->setLimit(nMaxPackets);
  }

  /** \brief get replacement policy
   */
  Policy*
  getPolicy() const
  {
    return m_policy.get();
  }

  /** \brief change replacement policy
   *  \pre size() == 0
   */
  void
  setPolicy(unique_ptr<Policy> policy);

  /** \brief get CS_ENABLE_ADMIT flag
   *  \sa https://redmine.named-data.net/projects/nfd/wiki/CsMgmt#Update-config
   */
  bool
  shouldAdmit() const
  {
    return m_shouldAdmit;
  }

  /** \brief set CS_ENABLE_ADMIT flag
   *  \sa https://redmine.named-data.net/projects/nfd/wiki/CsMgmt#Update-config
   */
  void
  enableAdmit(bool shouldAdmit);

  /** \brief get CS_ENABLE_SERVE flag
   *  \sa https://redmine.named-data.net/projects/nfd/wiki/CsMgmt#Update-config
   */
  bool
  shouldServe() const
  {
    return m_shouldServe;
  }

  /** \brief set CS_ENABLE_SERVE flag
   *  \sa https://redmine.named-data.net/projects/nfd/wiki/CsMgmt#Update-config
   */
  void
  enableServe(bool shouldServe);

public: // enumeration
  using const_iterator = Table::const_iterator;

  const_iterator
  begin() const
  {
    return m_table.begin();
  }

  const_iterator
  end() const
  {
    return m_table.end();
  }

private:
  std::pair<const_iterator, const_iterator>
  findPrefixRange(const Name& prefix) const;

  size_t
  eraseImpl(const Name& prefix, size_t limit);

  const_iterator
  findImpl(const Interest& interest) const;

  void
  setPolicyImpl(unique_ptr<Policy> policy);

NFD_PUBLIC_WITH_TESTS_ELSE_PRIVATE:
  void
  dump();


void
SetVirtualPayLoadSize(uint32_t n)
{
  m_virtualPayloadSize = n ;
}

private:
  Table m_table;
  unique_ptr<Policy> m_policy;
  signal::ScopedConnection m_beforeEvictConnection;

  uint32_t m_virtualPayloadSize;
  ns3::Time m_freshness;
  uint32_t m_signature;
  Name m_keyLocator;

  bool m_shouldAdmit = true; ///< if false, no Data will be admitted
  bool m_shouldServe = true; ///< if false, all lookups will miss
};

} // namespace cs

using cs::Cs;

} // namespace nfd

#endif // NFD_DAEMON_TABLE_CS_HPP
